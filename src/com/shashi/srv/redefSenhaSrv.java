package com.shashi.srv;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shashi.beans.UserBean;
import com.shashi.service.impl.UserServiceImpl;
import java.io.PrintWriter;

/**
 * Servlet implementation class LoginSrv
 */
@WebServlet("/redefSenhaSrv")
public class redefSenhaSrv extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public redefSenhaSrv() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String statusSenha = "";

        String senhaEnviada = request.getParameter("senhaEnviada");
        String emailId = request.getParameter("emailId");

        response.setContentType("text/html");
        
        UserServiceImpl dao = new UserServiceImpl();
        String senhaAtual = dao.getUserPassword(emailId);
        
        String novaSenha = request.getParameter("novaSenha");
        
        if (novaSenha != null) {
            boolean statusNovaSenha = dao.updateSenhaUserByEmail(emailId, novaSenha);
            if (statusNovaSenha) {
                PrintWriter pw = response.getWriter();
                
                RequestDispatcher rd = request.getRequestDispatcher("userProfile.jsp?statusSenha=" + statusSenha + "&novaSenha=" + novaSenha);                  
                rd.include(request, response); 
                
                pw.println("<script>alert('Nova senha redefinida com sucesso!')</script>");
            } else {
                PrintWriter pw = response.getWriter();
                
                RequestDispatcher rd = request.getRequestDispatcher("redefSenha.jsp?statusSenha=" + statusSenha);
                rd.include(request, response);
                
                pw.println("<script>alert('Não foi possível redefinir a senha!')</script>");
            }
            
        } else if (senhaAtual.equals(senhaEnviada) || senhaAtual == senhaEnviada) {
            statusSenha = "igual";
            
            RequestDispatcher rd = request.getRequestDispatcher("redefSenha.jsp?statusSenha=" + statusSenha);
            rd.forward(request, response);
            
        } else {
            statusSenha = "diferente";
                    
            RequestDispatcher rd = request.getRequestDispatcher("redefSenha.jsp?statusSenha=" + statusSenha);
            rd.forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }

}
