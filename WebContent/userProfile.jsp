<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page
    import="com.shashi.service.impl.*, com.shashi.service.*,com.shashi.beans.*,java.util.*,javax.servlet.ServletOutputStream,java.io.*"%>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Profile Details</title>
            <meta charset="utf-8">
            <script src="https://cdn.tailwindcss.com"></script>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet"
                  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
            <link rel="stylesheet" href="css/changes.css">
            <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
            <link rel="stylesheet"
                  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        </head>
        <body style="background-color: #94a3b8;">

            <%
                /* Checking the user credentials */
                String userName = (String) session.getAttribute("username");
                String password = (String) session.getAttribute("password");

                if (userName == null || password == null) {
                    response.sendRedirect("login.jsp?message=Session Expired, Login Again!!");
                }
                
                String senha = request.getParameter("novaSenha");

                UserService dao = new UserServiceImpl();
                UserBean user = dao.getUserDetails(userName, senha != null ? senha : password);
                if (user == null)
                    user = new UserBean("Test User", 98765498765L, "test@gmail.com", "ABC colony, Patna, bihar", 87659, "lksdjf");
                    
                String redefinirSenha = "redefinir";
            %>

            <jsp:include page="header_profile.jsp" />

            <div class="container bg-secondary py-10">
                <div class="flex flex-col items-center">
                    <div class="top-bar bg-white h-65 rounded-xl w-2/3 border border-gray-500">
                        <div class="title">
                            <div class="text-center">
                                <h1 class="text-2xl font-bold py-3 text-gray-500 ml-4">My profile</h1>
                            </div>
                        </div>
                    </div>
                    <div class="w-2/3 py-1 space-y-5">
                        <div
                            class="w-full flex flex-col bg-white rounded-lg border border-gray-500 justify-between py-10 px-5">

                            <div class="flex items-center mb-2">
                                <i class="fa-solid fa-user mr-2 fa-2x h-fit py-3"></i>
                                <span class="text-center">
                                    <%= user.getName()%>
                                </span>
                            </div>
                            <div class="flex items-center text-black-300 mb-2">
                                <i class="fa-solid fa-envelope mr-2 fa-2x h-fit py-3"></i>
                                <span class="text-center">
                                    <%= user.getEmail()%>
                                </span>
                            </div>
                            <div class="flex items-center mb-2">
                                <i class="fa-solid fa-square-phone text-black-300 mr-2 fa-2x h-fit py-3"></i>
                                <span class="text-center">
                                    <%= user.getMobile()%>
                                </span>
                            </div>
                            <div class="flex items-center mb-2">
                                <i class="fa-solid fa-location-dot mr-2 fa-2x h-fit py-3"></i>
                                <span class="text-center">
                                    <%= user.getAddress()%>
                                </span>
                            </div>
                            <div class="flex items-center mb-2">
                                <i class="fas fa-key mr-2 fa-2x h-fit py-3"></i>
                                <span class="text-center">
                                    <%= user.getPinCode()%>
                                </span>
                            </div>
                        </div>
                        <div class="w-fit flex justify-end py-1 w-full space-x-5">
                            <a  href="redefSenha.jsp?emailId=<%=user.getEmail()%>&redefinirSenha=<%=redefinirSenha%>" class="text-center font-bold border border-black bg-gray-500 w-32 py-4 rounded-lg text-gray-50 hover:border-black hover:bg-gray-500">
                                Reset Password
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="footer.html"%>

    </body>
</html>
