<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page
   import="com.shashi.service.impl.*, com.shashi.service.*,com.shashi.beans.*,java.util.*,javax.servlet.ServletOutputStream,java.io.*"
   %>
<!DOCTYPE html>
<html>
   <head>
      <title>Product Details</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
      <link rel="stylesheet" href="css/changes.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
      <link rel="stylesheet"
         href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   </head>
   <body style="background-color: #94a3b8;">
      <% /* Checking the user credentials */ String userName=(String) session.getAttribute("username"); String
         password=(String) session.getAttribute("password"); String userType=(String)
         session.getAttribute("usertype"); boolean isValidUser=true; if (userType==null || userName==null ||
         password==null || !userType.equals("customer")) { isValidUser=false; } ProductServiceImpl prodDao=new
         ProductServiceImpl(); List<ProductBean> products = new ArrayList<ProductBean>();
         
         	String search = request.getParameter("search");
         	String type = request.getParameter("type");
         	String message = "All Products";
         	if (search != null) {
         	products = prodDao.searchAllProducts(search);
         	message = "Showing Results for '" + search + "'";
         	} else if (type != null) {
         	products = prodDao.getAllProductsByType(type);
         	message = "Showing Results for '" + type + "'";
         	} else {
         	products = prodDao.getAllProducts();
         	}
         	if (products.isEmpty()) {
         	message = "No items found for the search '" + (search != null ? search : type) + "'";
         	products = prodDao.getAllProducts();
         	}
         	%>
      <jsp:include page="header_sem_search.jsp" />
      <div class="container">
         <h2>Products Details</h2>
         <table class="table table-bordered">
            <thead>
               <tr>
                  <th>Product Image</th>
                  <th>Product Name</th>
                  <th>Quantity</th>
                  <th>Value</th>
               </tr>
            </thead>
            <tbody>
               <%
                  CartServiceImpl cart = new CartServiceImpl();
                  List<CartBean> cartItems = new ArrayList<CartBean>();
                  cartItems = cart.getAllCartItems(userName);
                  double totAmount = 0;
                  for (CartBean item : cartItems) {
                  	String prodId = item.getProdId();
                  	int prodQuantity = item.getQuantity();
                  	ProductBean product = new ProductServiceImpl().getProductDetails(prodId);
                  	double currAmount = product.getProdPrice() * prodQuantity;
                  	totAmount += currAmount;
                  %>
               <img src=" ./ShowImage?pid=<%=product.getProdId()%>" alt="Product"
                  style="height: 150px; max-width: 180px">
               <p class="productname">
                  <%=product.getProdName()%>
               </p>
               <% String description=product.getProdInfo();
                  description=description.substring(0, Math.min(description.length(),
                  100)); %>
               <p class="productinfo">
                  <%=description%>..
               </p>
               <p class="price">
                  R$
                  <%=product.getProdPrice()%>
               </p>
               <%
                  }
                  %>
            </tbody>
         </table>
      </div>
      <%@ include file="footer.html" %>
   </body>
</html>