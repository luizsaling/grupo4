<%@page import="com.shashi.beans.UserBean"%>
<%@page import="com.shashi.service.impl.UserServiceImpl"%>
<%@page import="com.shashi.service.UserService"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://cdn.tailwindcss.com"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
        <title>Document</title>
        <script src="js/alert.js"></script>
    </head>

    <body style="background-color: #94a3b8;">

        <%
            /* Checking the user credentials */
            String userName = (String) session.getAttribute("username");
            String password = (String) session.getAttribute("password");

            if (userName == null || password == null) {
                response.sendRedirect("login.jsp?message=Session Expired, Login Again!!");
            }

            String redefinirSenha = request.getParameter("redefinirSenha");
            String emailId = request.getParameter("emailId");
            String statusSenhaIgual = request.getParameter("statusSenha");
            

            UserBean user = new UserBean();
            UserService dao = new UserServiceImpl();
            
            String senhaAtual = dao.getUserPassword(emailId);
            user = dao.getUserDetails(emailId, senhaAtual);
            
            int tentativa = 0;
        %>

        <%         
            // CASO A SENHA QUE ELE DIGITOU SEJA IGUAL A SENHA DO BANCO DE DADOS
            if (statusSenhaIgual != null && statusSenhaIgual.equals("igual")) {
        %>
        <jsp:include page="header_RedfSenha.jsp" />
        <div class="flex justify-end rounded-full">
            <div class="w-1/6 p-4 mb-4 text-sm text-red-800 rounded-md bg-green-50 dark:bg-gray-800 dark:text-green-400" role="alert" id="alertPassSucess">
                <span class="font-medium">Senha correta!</span>
            </div>
        </div>
        <div class="container bg-secondary py-10">
            <div class="flex flex-col items-center">
                <div class="top-bar bg-white h-65 rounded-xl w-2/4 border border-gray-500">
                    <div class="title">
                        <div class="text-center">
                            <h1 class="text-2xl font-bold py-3 text-gray-500 ml-4">Redefinir nova Senha</h1>
                        </div>
                    </div>
                </div>
                <div class="w-2/4 py-1 space-y-5">
                    <form action="./redefSenhaSrv?emailId=<%=emailId%>" method="post"
                          <div class="w-full flex flex-col bg-white rounded-lg border border-gray-500 justify-between py-4 px-5 h-44">
                        <div class="imput-box">
                            <label class="text-gray-500 font-bold" for="nome">Nova Senha</label>
                            <input id="novaSenha" name="novaSenha" type="text" placeholder=" Digite sua nova senha" required class="w-full h-8 border border-gray-500 rounded-md">
                        </div>
                        <div class="w-fit flex justify-end py-1 w-full space-x-5">
                            <a  href="userProfile.jsp" class="text-center font-bold border border-black bg-gray-500 w-20 py-2 rounded-lg text-gray-50  hover:border-black hover:bg-gray-400">
                                Cancelar
                            </a>
                            <button type="submit" class="btn btn-success border border-black bg-gray-500 w-20 py-2 rounded-lg text-gray-50 hover:border-black hover:bg-gray-400">
                                Confirmar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <%
            // CASO ELE SEJA REDIRECIONADO PARA A TELA DE CONFIRMA��O DE SENHA IGUAL
            } else {

                if (redefinirSenha == null) {
                    if (statusSenhaIgual.equals("diferente")) {
        %>
        <div class="flex justify-end rounded-full">
            <div class="w-1/6 p-4 mb-4 text-sm text-red-800 bg-red-50 dark:bg-gray-800 dark:text-red-400" role="alert" id="alertPassError">
                <span class="font-medium">Senha incorreta!</span> Tente novamente.
            </div>
        </div>

        <%
                }
            }
        %>

        <jsp:include page="header_RedfSenha.jsp" />

        <div class="container bg-secondary py-10">
            <div class="flex flex-col items-center">
                <div class="top-bar bg-white h-65 rounded-xl w-2/4 border border-gray-500">
                    <div class="title">
                        <div class="text-center">
                            <h1 class="text-2xl font-bold py-3 text-gray-500 ml-4">Confirme sua senha atual</h1>
                        </div>
                    </div>
                </div>
                <div class="w-2/4 py-1 space-y-5">
                    <form action="./redefSenhaSrv?emailId=<%=emailId%>" method="post"
                          <div class="w-full flex flex-col bg-white rounded-lg border border-gray-500 justify-between py-4 px-5 h-44">
                        <div class="imput-box">
                            <label class="text-gray-500 font-bold" for="nome">Senha Atual</label>
                            <input id="senha" name="senhaEnviada" type="text" placeholder=" Digite sua senha atual" required class="w-full h-8 border border-gray-500 rounded-md">
                        </div>
                        <div class="w-fit flex justify-end py-1 w-full space-x-5">
                            <a  href="userProfile.jsp" class="text-center font-bold border border-black bg-gray-500 w-20 py-2 rounded-lg text-gray-50  hover:border-black hover:bg-gray-400">
                                Cancelar
                            </a>
                            <button type="submit" class="text-center font-bold btn btn-success border border-black bg-gray-500 w-20 py-2 rounded-lg text-gray-50 hover:border-black hover:bg-gray-400">
                                Confirmar
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <%  
            }
        %>

    </body>
</html>
